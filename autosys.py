#!/bin/env python

"""
This script manages the usage of CA Autosys app by sending events (commands) 
to a predefined server, as described in sibling file '_asys.sh'.

Usage:

./autosys.py [-k] <[-j <job_name>] [-f <file_name>]> -a <action>

    -k: include this option (without value) if job needs to be killed 
        before applying the action.
    -j <job_name>: complete name of the job on which the action will be 
        executed. More than one job can be specified, separated by a comma (,).
    -f <file_name>: complete file path and name containing a list of jobs 
        on which the action will be executed. Every line in the file will 
        be treated as a complete job name.
    -a <action>: action to be executed. This is the list of actions accepted 
        by this script, and the event sent to Autosys:

        start   =>  STARTJOB
        kill    =>  KILLJOB
        delete  =>  DELETEJOB
        fstart  =>  FORCE_STARTJOB
        onice   =>  JOB_ON_ICE
        office  =>  JOB_OFF_ICE
        onhold  =>  JOB_ON_HOLD
        offhold =>  JOB_OFF_HOLD
        status  =>  [autostatus event]

    Please note options -j and -f are mutually excluded (this script will 
    execute whichever appears first), and at least one of them should be 
    specified.

    After execution of any event, this script will always print the status 
    of the job the event was executed on.

Dependency: this script depends on the sibling file '_asys.sh', 
licensed/copyrighted by CA.
"""

import getopt
from os import path
import sys
import time

import subprocess

EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
LOG_FILE = path.join(EXEC_DIR, 'logs', 'autosys.log')
CA_EXE_FILE = path.join(EXEC_DIR, '_asys.sh')

sys.path.append(path.dirname(EXEC_DIR))
from pssapi.logger import logger as lg

ACTIONS = {
    "start": "STARTJOB",
    "kill": "KILLJOB",
    "delete": "DELETEJOB",
    "fstart": "FORCE_STARTJOB",
    "onice": "JOB_ON_ICE",
    "office": "JOB_OFF_ICE",
    "onhold": "JOB_ON_HOLD",
    "offhold": "JOB_OFF_HOLD",
    "status": ""
}
COMMAND = ". {ca_file}; {command}"
logger = None
OPTIONS = "j:f:a:k"

def _output(text, prnt=True):
    """Log text and print it out if 'prnt' is True."""
    if prnt:
        print(text)
    logger.info(text)

def _quit_script(err=None):
    """Print err and quit this script."""
    if err:
        _output("ERROR - {0}".format(err))
    _output("Quitting now... bye.")
    exit()

def _parse_options(options):
    """Parse list 'options' to keep either option -f or -j. Return dict."""
    opts = {}
    current_keys = []
    for option in options:
        if (option[0] == '-f' and '-j' in current_keys) or \
           (option[0] == '-j' and '-f' in current_keys):
           continue
        current_keys.append(option[0])
        opts[option[0]] = option[1]
    if '-j' not in current_keys and '-f' not in current_keys \
            or '-a' not in current_keys:
        _quit_script("Missing parameter.")
    return opts

def _send_event(job, command):
    """Run 'command' on the Autosys server."""
    _output("Sending this event: {0}".format(command), prnt=False)
    p = subprocess.Popen(command, stdin=subprocess.PIPE, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    err = stderr.strip()
    output = stdout.strip()
    if err:
        _output("Error: {0}: {1}".format(job, err))
    if output:
        _output("{0}: {1}".format(job, output))

def _get_command(action, job, comment):
    """Create command to be run on Autosys server."""
    if action:
        command = ("sendevent -J {job} -E {action} -C {comment} -P 1").\
            format(job=job, action=action, comment=comment)
    else:
        command = "autostatus -J {job}".format(job=job)
    return COMMAND.format(ca_file=CA_EXE_FILE, command=command)

def _kill_job(job, comment):
    """Send event to kill 'job'."""
    command = _get_command(ACTIONS['kill'], job, comment)
    _send_event(job, command)
    time.sleep(1)  # to give time so the status can give an accurate result

def _jobs_from_file(f):
    """Return a list of jobs comming on file 'f'."""
    jobs = []
    with open(f) as _f:
        for entry in _f.readlines():
            _entry = entry.strip()
            if _entry:
                jobs.append(_entry)
    return jobs

def _apply_options(opts):
    """Execute the options over the jobs specified on 'opts'."""
    opts_keys = opts.keys()
    action = ACTIONS.get(opts['-a'], None)
    if action == None:
        _quit_script("Action not available.")
    jobs = opts['-j'].split(',') if '-j' in opts_keys \
                                 else _jobs_from_file(opts['-f'])
    comment = "\"Event sent from PSS/autosys.py\""
    _output("Jobs: {0}".format(jobs), prnt=False)
    for job in jobs:
        if action == ACTIONS['kill'] or '-k' in opts_keys:
            _kill_job(job, comment)
        if action != ACTIONS['kill']:
            command = _get_command(action, job, comment)
            _send_event(job, command)
        # show status after executing original status
        if action != ACTIONS['status']:
            command = _get_command(ACTIONS['status'], job, comment)
            _send_event(job, command)

def _main():
    global logger
    logger = lg.load_logger(log_file_name=LOG_FILE, cgi=True)
    fullparams = sys.argv
    _output("I was called this way: {0}".format(fullparams), prnt=False)
    params = fullparams[1:]
    try:
        options, values = getopt.gnu_getopt(params, OPTIONS)
        if values:
            _quit_script("Parameters incorrect.")
    except Exception as e:
        _quit_script(str(e))
    opts = _parse_options(options)
    _apply_options(opts)
    _quit_script()

if __name__ == "__main__":
    _main()
