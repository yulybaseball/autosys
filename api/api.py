"""
This script is intended to be the API for an interface of Autosys.

Exposed services are these (please check every function's documentation):

retrieve_job_data(job_name)
run_job(job_name)
stop_job(job_name)

Dependency: this script depends on the file '../_asys.sh', 
licensed/copyrighted by CA.
"""

from os import path
import subprocess

# assuming the CA script for Autosys is 1 level above this script
CA_EXE_FILE = path.join(path.dirname(path.dirname(path.realpath(__file__))), 
                        '_asys.sh')
# Autosys script
CMD_AUTOSYS_MANAGER = ". {autosys_script}; ".format(autosys_script=CA_EXE_FILE)
# Autosys commands
CMD_AUTOREP = "autorep -j {job_name}"
CMD_AUTOREP_Q = "autorep -j {job_name} -q"
CMD_AUTOSTATUS = "autostatus -J {job_name}"
CMD_RUN = "sendevent -J {job_name} -E FORCE_STARTJOB -C \"From PSS API\" -P 1"
CMD_STOP = "sendevent -J {job_name} -E KILLJOB -C \"From PSS API\" -P 1"
# Autosys job's status
AUTOSYS_STATUS = {
    "AC": "ACTIVATED",
    "FA": "FAILURE",
    "IN": "INACTIVE",
    "OH": "ON_HOLD",
    "OI": "ON_ICE",
    "QU": "QUE_WAIT",
    "RE": "RESTART",
    "RU": "RUNNING",
    "ST": "STARTING",
    "SU": "SUCCESS",
    "TE": "TERMINATED"
}
# not allowed to run job when on one of these status
NOT_ALLOWED2RUN = ["QUE_WAIT", "RUNNING", "STARTING"]

def _get_job_status_text(status):
    """Return the text of the status, or UNKNOWN if it's not defined."""
    return AUTOSYS_STATUS.get(status.upper(), "UNKNOWN")

def _run_command(command, include_autosys_script=True):
    """Run command and return the error and the output."""
    if include_autosys_script:
        command = "{0}{1}".format(CMD_AUTOSYS_MANAGER, command)
    p = subprocess.Popen(command, stdin=subprocess.PIPE, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    return (stderr.strip(), stdout.strip())

def _current_job_status(job_name):
    """Retrieve and return the current job status in Autosys server."""
    command = CMD_AUTOSTATUS.format(job_name=job_name)
    error, output = _run_command(command)
    if error:
        raise Exception(error)
    return output

def _allowed2run(status):
    """Return whether a job in status is allowed to run."""
    return status not in NOT_ALLOWED2RUN

def _parse_job_complete_data(main_data, additional_data):
    """Parse strings so job data can be formatted into a dict.

    Keyword arguments:
    main_data -- result from executing Autosys command 'autorep', which result 
        is a string needed to be parsed out
    additional_data -- result from executing Autosys command 'autorep -q', 
        which result is a string needed to be parsed out

    Return a dictionary with all the job information.
    """
    complete_data = {}
    # main data
    md = main_data.split('\n')[-1].split()
    last_start = "{0} {1}".format(md[1], md[2])
    complete_data['last_start'] = last_start
    if '---' in md[3]:
        # this should happen if job is running or starting to run
        # in which cases there is no last_end
        last_end = md[3]
        status =_get_job_status_text(md[4])
    else:
        last_end = "{0} {1}".format(md[3], md[4])
        status =_get_job_status_text(md[5])
    complete_data['last_end'] = last_end
    complete_data['status'] = status
    # additional data
    ad = additional_data.split('\n')
    imp_keys = ['days_of_week', 'start_mins', 'run_window', 'start_times']
    # parse strings like these: 
    #   start_mins: 0,30
    #   days_of_week: mo,tu,we,th,fr
    #   run_window: "8:30-18:30"
    for _ad in ad:
        _ad_key = _ad.split(':')
        if _ad_key[0] in imp_keys:
            complete_data[_ad_key[0]] = ":".join(_ad_key[1:]).strip().\
                                        replace('"', '')
    # add the missing keys
    missingkeys = list(set(imp_keys) - set(complete_data.keys()))
    for missk in missingkeys:
        complete_data[missk] = ""
    # allowed to run?
    complete_data['allowed2run'] = _allowed2run(status)
    return complete_data

class AutosysAPI:
    """Define the available actions that can be requested from the frontend."""

    def retrieve_job(self, job_name):
        """Retrieve information from a job in Autosys.

        Keyword arguments:
        job_name -- name of the job to retrieve information from.

        Return a dictionary with the job information. If error or exception 
        arises, the text of it is returned instead.
        """
        try:
            # get main job info
            command = CMD_AUTOREP.format(job_name=job_name)
            error, output = _run_command(command)
            if error:
                raise Exception(error)
            main_data = output
            # get additional job info
            command = CMD_AUTOREP_Q.format(job_name=job_name)
            error, output = _run_command(command)
            if error:
                raise Exception(error)
            return _parse_job_complete_data(main_data, output)
        except IndexError:
            return {
                "error": 
                "Output from executing a command in Autosys is not " + 
                "returning the expected data."
            }
        except Exception as e:
            return {"error": str(e)}

    def run_job(self, job_name):
        """Force start a job in Autosys.

        Keyword arguments:
        job_name -- name of the job to be forced to start.

        Return a dictionary with the result of sending the command to Autosys 
        for trying to start a job. If error or exception arises, the text 
        of it is returned instead.
        """
        try:
            # get current job status on Autosys server
            # this is done to prevent trying to run a job when it's on a
            # status which is not allowd to be run
            current_status = _current_job_status(job_name)
            if not _allowed2run(current_status):
                er = ("Job is currently in status {0}, so it's not " + 
                     "allowed to run.").format(current_status)
                raise Exception(er)
            command = CMD_RUN.format(job_name=job_name)
            error, output = _run_command(command)
            if error:
                raise Exception(error)
            return {"running_result": output}
        except Exception as e:
            return {"error": str(e)}

    def stop_job(self, job_name):
        """Stop a job in Autosys.

        Keyword arguments:
        job_name -- name of the job to be stopped.

        Return a dictionary with the result of sending the command to Autosys 
        for trying to stop a job. If error or exception arises, the text of 
        it is returned instead.
        """
        try:
            command = CMD_STOP.format(job_name=job_name)
            error, output = _run_command(command)
            if error:
                raise Exception(error)
            return {"stopping_result": output}
        except Exception as e:
            return {"error": str(e)}
