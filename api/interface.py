#!/bin/env python

"""
This script is intended to be the the middle tier between an interface of 
Autosys and an API for Autosys.

Usage:

./autosys.py -loadjobs
./autosys.py <action> <job_name> <requester_ip>

- loadjobs: indicates this script will print the json containing the jobs 
    to be shown in the web page
- action: action to be applied to the job. So far, one of these 3:
    retrieve: get data of the job
    run: run the job
    stop: stop the job
- job_name: name of the job to apply the action over
- requester_ip: ip requesting the action
"""

import json
from os import path
import sys

EXEC_DIR = path.dirname(path.dirname(path.realpath(sys.argv[0])))
LOG_FILE = path.join(EXEC_DIR, 'logs', 'api.log')
CONF_FILE = path.join(EXEC_DIR, 'conf', 'jobs.json')
CA_EXE_FILE = path.join(EXEC_DIR, '_asys.sh')

sys.path.append(path.dirname(EXEC_DIR))
from pssapi.logger import logger as lg
from pssapi.utils import conf

logger = None
OPTIONS = ["-loadjobs"]

def _return_json2frontend(result):
    """Common output to the frontend, so the result is valid JSON content."""
    print(json.dumps(result))

def _check_input(params):
    """Check parameters are coming this way:
        <action> <job_name> <requester_ip>
    """
    error = ""
    try:
        params_length = len(params[1:])
        if params_length == 1 and params[1] not in OPTIONS:
            raise ValueError
        if params_length != 3 and params_length != 1:
            raise IndexError
    except IndexError:
        error = "Number of parameters is not expected."
    except ValueError:
        error = "Undefined/unexpected option '{0}'.".format(params[1])
    return error

def _load_jobs():
    """Fetch and return the list of jobs defined in the conf file."""
    try:
        jobs_list = conf.get_conf(CONF_FILE)
        _return_json2frontend({"jobs": sorted(jobs_list)})
    except Exception as e:
        error = str(e)
        logger.error(error)
        _return_json2frontend({"error": error})

def _call_api(action, job_name):
    """Call Autosys API and print result."""
    from api import AutosysAPI
    autosysAPI = AutosysAPI()
    method2exe = "{action}_job".format(action=action)

    def _undefined_action(self):
        error = "API has no action '{0}'".format(action)
        logger.error(error)
        return {"error": error}

    result = getattr(autosysAPI, method2exe, _undefined_action)(job_name)
    if 'error' in result:
        logger.error(result['error'])
    _return_json2frontend(result)

def _main():
    global logger
    logger = lg.load_logger(log_file_name=LOG_FILE, cgi=True)
    fullparams = sys.argv
    logger.info("Request: {0}".format(" ".join(fullparams)))
    error = _check_input(fullparams)
    if error:
        logger.critical(error)
        _return_json2frontend({"error": error})
    else:
        action = fullparams[1]
        if action in OPTIONS:
            _load_jobs()
        else:
            job_name = fullparams[2]
            _call_api(action, job_name)

if __name__ == "__main__":
    _main()
    exit(0)
